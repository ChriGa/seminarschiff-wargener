<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 9;
if ($this->countModules('left') || $this->countModules('right')) { 
	$span = 6;
} else {
	$span = 12;
}

?>
<div class="clear-container">
	<div class="container content">
			<jdoc:include type="modules" name="banner" style="xhtml" />
		<div class="row-fluid">
		
			<?php if ($this->countModules('left')) : ?>
				<div class="span6 clear-left">
					<jdoc:include type="modules" name="left" style="xhtml" />
				</div>				
			<?php endif; ?>
		<?php usleep(750000) // CG usleep function für SVG Animation in der Index ?>
			<main id="content" role="main" class="span<?php echo $span; ?>">
				<!-- Begin Content -->
				<jdoc:include type="modules" name="position-3" style="xhtml" />
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				<jdoc:include type="modules" name="position-2" style="none" />
				<!-- End Content -->
			</main>
			
			<?php if ($this->countModules('right')) : ?>
				<div class="span6 clear-right">		
					<jdoc:include type="modules" name="right" style="xhtml" />
				</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>