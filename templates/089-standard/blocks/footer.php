<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="clear-footer" role="contentinfo">
	<div class="container clear-footer-wrap">				
		<div class="row-fluid">								
			<div class="footer">
				<jdoc:include type="modules" name="footer" style="none" />
			</div>
			<?php if ($this->countModules('footnav')) : ?>
			<div class="footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>		
		</div>	
	</div>
</footer>
	
		