<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news BOOTSTRAP CAROUSEL MOD by cg@089webdesign.de
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


require_once JURI::root(true).'seminar_dev/modules/mod_menu/helper.php';
$base      = ModMenuHelper::getBase($params);
$path      = $base->tree;

?>
<div id="myCarousel" class="carousel slide newsflash<?php echo $moduleclass_sfx; ?>">
	<ol class="carousel-indicators">
		<?php $x = 0; 
			while($list[$x]) {
				print '<li data-target="#myCarousel" data-slide-to="'.$x.'" class="active"></li>';
				$x ++; } ?>
	</ol>
  	<div class="carousel-inner">
  	<?php 
		print (in_array($item->id, $path)) ? $activeItem .= ' active' : " " ?>
			<?php foreach ($list as $item) : ?>
			    <div class="<?=$activeItem ?> item">
					<?php require JModuleHelper::getLayoutPath('mod_articles_news', '_item089'); ?>
				</div>
			<?php endforeach; ?>
	</div>
	<?
	/* CG: Bei Bedarf die a-tags einkommentieren für Navigation
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
	 */
	?>
</div>
<script type="text/javascript">
	jQuery('.carousel').carousel({
		  interval: 3000
		})
</script>
<? //preprint($list[1]); ?>
