<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class;
?>">
	<!-- Body -->
		<div id="pagewrap" class="site_wrapper pagewrap animsition">
			<div class="" id="page-1">
				<section id="header-section">
				<?php
					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
					// including menu
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php'); ?>
					<?php if ($this->countModules('flPost')) : ?>
							<jdoc:include type="modules" name="flPost" style="custom" />
					<?php endif; ?>				
				</section>
				<section id="content-section">
				<?php
					// including breadcrumbs
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');								
					// including content
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	?>
				</section>
				<section id="bottom-section">
				<?php
					// including bottom
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php'); ?>	
				</section>
				<?php						
					// including footer
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php'); ?>
			</div>
		</div>
	<jdoc:include type="modules" name="debug" style="none" />
			<div id="loader" class="pageload-overlay" data-opening="m -5,-5 0,70 90,0 0,-70 z m 5,35 c 0,0 15,20 40,0 25,-20 40,0 40,0 l 0,0 C 80,30 65,10 40,30 15,50 0,30 0,30 z">
				<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 80 60" preserveAspectRatio="none" >
					<path d="m -5,-5 0,70 90,0 0,-70 z m 5,5 c 0,0 7.9843788,0 40,0 35,0 40,0 40,0 l 0,60 c 0,0 -3.944487,0 -40,0 -30,0 -40,0 -40,0 z"/>
				</svg>
			</div>
<script src="<?= 'templates/'. $this->template . '/js/classie.js'?>"></script>
<script src="<?= 'templates/'. $this->template . '/js/svgLoader.js' ?>"></script>
	<script>
		(function() {
			var pageWrap = document.getElementById( 'pagewrap' ),
				pages = [].slice.call( pageWrap.querySelectorAll( 'div.container' ) ),
				currentPage = 0,
				triggerLoading = [].slice.call( pageWrap.querySelectorAll( 'a.pageload-link' ) ),
				loader = new SVGLoader( document.getElementById( 'loader' ), { speedIn : 300, easingIn : mina.easeinout } );

			function init() {
				triggerLoading.forEach( function( trigger ) {
					trigger.addEventListener( 'click', function( ev ) {
						//ev.preventDefault();
						loader.show();
						// after some time hide loader
						setTimeout( function() {
							loader.hide();

							classie.removeClass( pages[ currentPage ], 'show' );
							// update..
							currentPage = currentPage ? 0 : 1;
							classie.addClass( pages[ currentPage ], 'show' );

						}, 700 );
					} );
				} );	
			}
		//event.preventDefault();
			init();
		})();
	</script>	
	<script type="text/javascript">
		jQuery(document).ready(function() {
		  jQuery(".animsition").animsition({
		    inClass: 'zoom-in-sm',
		    outClass: 'zoom-out-sm',
		    inDuration: 300,
		    outDuration: 100,
		    linkElement           :   '.animsition-link',
		    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		    loading               :    false,
		    loadingParentElement  :   'body', //animsition wrapper element
		    loadingClass          :   'animsition-loading',
		    unSupportCss          : [ 'animation-duration',
		                              '-webkit-animation-duration',
		                              '-o-animation-duration'
		                            ],
			browser: [ 'animation-duration', '-webkit-animation-duration'],	                            
		    <? /*"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration". */ ?>
		    
		    overlay               :   false,
		    
		    overlayClass          :   'animsition-overlay-slide',
		    overlayParentElement  :   'body',
		    transition: function(url){ window.location.href = url; }
		  });
		});
	</script>	
</body>
</html>
