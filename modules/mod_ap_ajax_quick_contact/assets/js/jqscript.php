<?php 
/**
 * @package 	mod_ap_ajax_quick_contact.php - AP Ajax Quick Contact Module
 * @version		3.3
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

//no direct access
defined('_JEXEC') or die;
?>
<script type="text/javascript">
jQuery(document).ready(function(){  
	jQuery('form#ap_<?php echo $uniqid;?>').submit(function() { 
		<?php // bring loader to the scene ?>
		jQuery('form#ap_<?php echo $uniqid;?> .ap-btn span').prepend("<i class='fa fa-spinner fa-spin'></i>").fadeIn(400);
		<?php // icon with text in submit button ?>
		jQuery('form#ap_<?php echo $uniqid;?> .ap-btn i').removeClass('fa-share');
		jQuery('form#ap_<?php echo $uniqid;?> .error').remove();
		var hasError = false;
		jQuery('form#ap_<?php echo $uniqid;?> .requiredField').each(function() {
			if(jQuery.trim(jQuery(this).val()) == '') {		  
				var labelText = jQuery(this).prev('label').text().replace(':', '');<?php // removes : at the end ?>
				if(jQuery(this).hasClass('name')) {
				var labelText = jQuery(this).prev('label').text();
				hasError = true;
				}	
				jQuery(this).parent().append('<div class="error"><?php echo $ap_error_field;?> '+labelText+'!</div>');
				
				jQuery('form#ap_<?php echo $uniqid;?> .error').fadeIn(500);
				jQuery(this).addClass('invalid');
				jQuery('form#ap_<?php echo $uniqid;?> .ap-btn i').addClass('fa-share');
				jQuery('form#ap_<?php echo $uniqid;?> .ap-btn span i.fa-spinner').remove();
				hasError = true;
			} else if(jQuery(this).hasClass('email')) {
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim(jQuery(this).val()))) {
					var labelText = jQuery(this).prev('label').text();
					jQuery(this).parent().append('<div class="error"><?php echo $ap_error_email;?></div>');
					jQuery('.error').fadeIn(500);
					jQuery('form#ap_<?php echo $uniqid;?> .ap-btn i').addClass('fa-share');
					jQuery('form#ap_<?php echo $uniqid;?> .ap-btn span i.fa-spinner').remove();
					jQuery(this).addClass('invalid');
					hasError = true;
				}											
			}	
		});<?php if ($captcha_label == "1") {?>
		jQuery('form#ap_<?php echo $uniqid;?> .requiredCaptcha').each(function() {
			if(jQuery.trim(jQuery(this).val()) != '<?php echo $_SESSION['expect'];?>') {
				var labelText = jQuery(this).prev('label').text();
				jQuery(this).parent().append('<div class="error"><?php echo $ap_captchaError;?></div>');
				jQuery('form#ap_<?php echo $uniqid;?> .error').fadeIn(500);
				jQuery('form#ap_<?php echo $uniqid;?> .ap-btn i').addClass('fa-share');
				jQuery('form#ap_<?php echo $uniqid;?> .ap-btn span i.fa-spinner').remove();
				jQuery(this).addClass('invalid');
				hasError = true;
		}});<?php } ?>
		if(!hasError) {
			var formInput = jQuery(this).serialize();
			jQuery.post(jQuery(this).attr('action'),formInput, function(data){
				jQuery('form#ap_<?php echo $uniqid;?>').fadeOut(300, function() {	
					jQuery(this).before('<div class="success fade in"><a class="close hasTooltip" title="Close" data-dismiss="alert" href="#">&times;</a><?php echo $ap_send_message=='' ? JText::_(SENDMESSAGE) : $ap_send_message; ?></div>');
					});
			jQuery('form#ap_<?php echo $uniqid;?> .fa-share','form#ap_<?php echo $uniqid;?> .error','form#ap_<?php echo $uniqid;?> .loader','form#ap_<?php echo $uniqid;?> .ap-btn span i.fa-spinner').remove();
			});
		}
		return false;	
	});
});
</script>